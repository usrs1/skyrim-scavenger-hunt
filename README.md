# Skyrim Scavenger Hunt

This website is a simple tool that for scavenger hunts my friends and I compete in. After the player puts in the parameters of the scavenger hunt. Lists of tasks, of varying difficultly, are pulled from the database and presented to the competitor. Tasks can be checked off the list, and the players total score is calculated and shown on screen.

### List of *Potential* Future Features:

- List of competitors can be added as a game parameter. Allowing the person moderating the game to tally points instead of the players.
- Allow user generated Scavenger hunt items to be added.
 - User accounts and/or a Captcha system would probably have to be installed to avoid abuse.
- Animation that pops up when a player reaches the winning score (needs to be a toggle in the game setup)

### Developers & Tools Used

This Project is developed by Nathan McNeil, if you enjoyed this tool or have any feedback, let me know by sending me a message on twitter or via email, I love feedback.

Tools Used:
- NodeJs
- Express
- MySQL
- Materialize
